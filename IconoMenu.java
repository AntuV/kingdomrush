import java.util.Vector;
import java.awt.*;
import javax.imageio.*;
import com.entropyinteractive.*;
import java.awt.image.*;

public class IconoMenu extends ObjetoGrafico{
	
	private BufferedImage animacion;
	private int tipo;
	private PuntoEstrategico punto;

	public IconoMenu(int tipo, PuntoEstrategico p){
		super();
		this.tipo = tipo;
		switch(tipo){
			case 0:
				setObjeto("icono_torre_arquero.png", p.getX()-50+9, p.getY()-50+11);
				try {
					animacion = ImageIO.read(getClass().getResource("imagenes/icono_torre_arquero1.png"));
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case 1:
				setObjeto("icono_torre_barraca.png", p.getX()-50+90, p.getY()-50+11);
				try {
					animacion = ImageIO.read(getClass().getResource("imagenes/icono_torre_barraca1.png"));
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case 2:
				setObjeto("icono_torre_mago.png", p.getX()-50+9, p.getY()-50+88);
				try {
					animacion = ImageIO.read(getClass().getResource("imagenes/icono_torre_mago1.png"));
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case 3:
				setObjeto("icono_torre_mortero.png", p.getX()-50+90, p.getY()-50+88);
				try {
					animacion = ImageIO.read(getClass().getResource("imagenes/icono_torre_mortero1.png"));
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case 4:
				setObjeto("icono_torre_mejorar.png", p.getX(), p.getY()-50);
				try {
					animacion = ImageIO.read(getClass().getResource("imagenes/icono_torre_mejorar1.png"));
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case 5:
				setObjeto("icono_torre_vender.png", p.getX(), p.getY()+50);
				try {
					animacion = ImageIO.read(getClass().getResource("imagenes/icono_torre_vender1.png"));
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;				
		}
		punto = p;
	}

	public void dibujar(Graphics2D g, Mouse m){
		if (this.contiene(m)) {
			g.drawImage(animacion, x-imagen.getWidth()/2,y-imagen.getHeight()/2, null);
		} else {
			g.drawImage(imagen, x-imagen.getWidth()/2,y-imagen.getHeight()/2, null);
		}
	}

	public void clic(Mouse m) {
		if(contiene(m)) {
			if (tipo <= 3 && tipo >= 0) {
				punto.setTorre(tipo);
			} else if (tipo == 4) {
				punto.mejorarTorre();
			} else {
				punto.venderTorre();
			}
			Musica.reproducir("menuclic.wav");
		}
	}
}