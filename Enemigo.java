import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.Graphics2D;
import java.util.List;
import java.util.Random;
import java.util.Vector;

public class Enemigo extends Movil {

	private	double indice = 0;
	private int flag;
	private Point2D posicion = new Point2D.Float(310 - 8, 0 - 8);
	private Point2D posicionflag = new Point2D.Float();
	protected Boolean atacar = false;
	protected Boolean detenido = false;
	protected Boolean tieneSoldado = false;
	protected Boolean visible;
	protected double creacion;
	protected Integer carril;
	protected Integer energia;
	protected Integer fuerza;
	protected Integer penalizacion;
	protected Integer recompensa;
	protected Integer velocidad;
	protected Soldado soldado;
	protected String direccion;

	public Enemigo(String ruta){
		super(ruta, 0, 0);
		visible = false;
		flag = 0;
		carril = -1;
		creacion = (double) System.nanoTime() / 1000000000.0;
	}

	public void atacar(){
		if (tieneSoldado) {
			atacar = true;
			if (((double) System.nanoTime() / 1000000000.0) - creacion >= 1.4) {
				creacion = (double) System.nanoTime() / 1000000000.0;
				if (soldado.getEnergia() > 0) {
					soldado.setEnergia(fuerza);
				} else {
					atacar = false;
					detenido = false;
					soldado = null;
				}
			}
		} else {
			atacar = false;
			detenido = false;
			soldado = null;
		}
	}

	public void destruir(){}

	public void mover(List<Point2D> puntos) {
		if (!detenido) {
		    indice += (0.0005 * velocidad);
		    int index = Math.min(Math.max(0, (int) (puntos.size() * indice)), puntos.size() - 1);
		    posicion = puntos.get(index);
		    indice = (index >=puntos.size() - 1) ? 0.0 : indice;

		    if (flag == 60) { // detecta direccion
		    	posicionflag.setLocation(posicion.getX(), posicion.getY());
		    	direccion = String.valueOf((Direccion.getDirection((float)x, (float)y, (float)posicion.getX(), (float)posicion.getY())));
		    	flag = 0;
		    } else if (flag == 0) {
		    	posicionflag.setLocation(posicion.getX(), posicion.getY());
		    	flag++;
		    } else {
		    	flag++;
		    }
		    x = (int)posicion.getX();
		    y = (int)posicion.getY();
		}
	}

	public Boolean intersecta(Vector<Meta> metas) {
		return metas.get(carril).getPunto().equals(new Point2D.Double(x, y)); //me devuelve el punto de uno de los carriles de mi vector meta y lo compara con el de el enemigo  
	}

    public Integer getEnergia() {
    	if(energia == 0) {
    		if (soldado != null) {
	    		soldado.quitarAtacar();
	    	}
    	}
        return this.energia;
    }

    public void setEnergia(int energia) {
        this.energia = (this.energia - energia < 0) ? 0 : this.energia - energia;
    }

    public Boolean getVisible() {
        return visible;
    }


    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Integer getRecompensa() {
        return this.recompensa;
    }


    public int getCarril(int carriles){
    	if (carril == -1) {
    		Random r = new Random();
    		carril = r.nextInt(carriles);
    	}
    	return carril;
    }

    public void detener(){
    	detenido = true;
    }

    public void quitarDetener(){
    	detenido = false;
    }

    public void setSoldado(Soldado s){
    	soldado = s;
    }

    public void liberarSoldado(){
    	soldado.quitarAtacar();
    }

    public Boolean tieneSoldado(){
    	return soldado != null;
    }

    public Integer getPenalizacion(){
    	return this.penalizacion;
    }
}
