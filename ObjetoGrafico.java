import com.entropyinteractive.* ;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.io.InputStream;
import java.util.Vector;
import javax.imageio.*;
import javax.sound.sampled.*;

public class ObjetoGrafico {

	protected int x;
	protected int y;
	protected BufferedImage imagen;
	protected Rectangle2D area;

	public ObjetoGrafico(){
		imagen = null;
		x = 0;
		y = 0;
		area = null;
	}

	public ObjetoGrafico(int x, int y, int ancho, int alto){
		this.x = x;
		this.y = y;
		area = new Rectangle2D.Double(x - ancho/2, y - alto/2, ancho, alto);
	}

	public ObjetoGrafico(int x, int y){
		this.x = x;
		this.y = y;
	}

	public ObjetoGrafico(String ruta, int x, int y){
		try {
			imagen = ImageIO.read(getClass().getResource("imagenes/" + ruta));
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.x = x;
		this.y = y;
		area = new Rectangle2D.Double(x - imagen.getWidth()/2, y - imagen.getHeight()/2, imagen.getWidth(), imagen.getHeight());
	}

	public boolean contiene(Mouse m){
		return (area.contains(m.getX(), m.getY()));
	}

	public void dibujar(Graphics2D g){
		g.drawImage(imagen,x,y,null);
	}

	public int getX(){
		return this.x;
	}

	public int getY(){
		return this.y;
	}

	public void setX(int x){
		this.x = x;
	}

	public void setY(int y){
		this.y = y;
	}

	public int getAncho(){
		return (int)area.getWidth();
	}

	public int getAlto(){
		return (int)area.getHeight();
	}

	public BufferedImage getImagen(){
		return this.imagen;
	}

    public void sonido(String ruta, int volumen) {
    	FloatControl gainControl;
        Clip clip = null;
        try {
            InputStream is = getClass().getResourceAsStream("sonido/" + ruta);
            AudioInputStream inputStream = AudioSystem.getAudioInputStream(is);
            DataLine.Info info = new DataLine.Info(Clip.class, inputStream.getFormat());
            clip = (Clip)AudioSystem.getLine(info);
            clip.open(inputStream);
        } catch (Exception ex) {
            System.out.println(ex);
        }
        gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN); 
        float vol = (float) volumen / 100;
    	gainControl.setValue(20f * (float) Math.log10(vol));
        clip.setFramePosition(0);
        clip.start();
    }

	public void setImagen(String ruta){
		try {
			imagen = ImageIO.read(getClass().getResource("imagenes/" + ruta));
		} catch (Exception e) {
			e.printStackTrace();
		}
		area = new Rectangle2D.Double(x - imagen.getWidth()/2, y - imagen.getHeight()/2, imagen.getWidth(), imagen.getHeight());
	}

	public void setObjeto(String ruta, int x, int y){
		try {
			imagen = ImageIO.read(getClass().getResource("imagenes/" + ruta));
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.x = x;
		this.y = y;
		area = new Rectangle2D.Double(x - imagen.getWidth()/2, y - imagen.getHeight()/2, imagen.getWidth(), imagen.getHeight());
	}

	public void setArea(){
		area = new Rectangle2D.Double(x - imagen.getWidth()/2, y - imagen.getHeight()/2, imagen.getWidth(), imagen.getHeight());
	}

	public Rectangle2D getArea(){
		return this.area;
	}

	public void setAreaRel(int x, int y, int ancho, int alto){
		area = new Rectangle2D.Double(this.x - imagen.getWidth()/2 + x, this.y - imagen.getHeight()/2 + y, imagen.getWidth()+ancho, imagen.getHeight()+alto);
	}
}