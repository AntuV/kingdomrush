import java.util.Vector;

public interface IAtaque {
	public void atacar(Vector<Municion> municiones, Enemigo e);
}