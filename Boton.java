import javax.imageio.*;
import java.awt.image.*;
import com.entropyinteractive.Mouse;
import java.awt.*;

public class Boton extends ObjetoGrafico {
	
	private BufferedImage animacion;
	private Boolean sono;

	public Boton(String ruta, int x, int y, String rutaAnimacion){
		super(ruta, x, y);
		try {
			this.animacion = ImageIO.read(getClass().getResource("imagenes/" + rutaAnimacion));
		} catch(Exception e){}
		sono = false;
	}

	public void mover(int x, int y){
		setX(x);
		setY(y);
		setArea();
	}

	public void dibujar(Graphics2D g, Mouse m){
		if (contiene(m)) {
			g.drawImage(animacion, this.getX()-imagen.getWidth()/2, this.getY()-imagen.getHeight()/2, null);
			if (!sono) Musica.reproducir("mousehover.wav");
			sono = true;
		} else {
			g.drawImage(imagen, x-imagen.getWidth()/2, y-imagen.getHeight()/2, null);
			sono = false;
		}
	}

}