public class Direccion {
    public static Direction getDirection(float x1, float y1, float x2, float y2){
        double angle = getAngle(x1, y1, x2, y2);
        return Direction.get(angle);
    }

    public static double getAngle(float x1, float y1, float x2, float y2) {

        double rad = Math.atan2(y1-y2,x2-x1) + Math.PI;
        return (rad*180/Math.PI + 180)%360;
    }

    public enum Direction{
        arriba,
        abajo,
        izquierda,
        derecha;

        public static Direction get(double angle){
            if(inRange(angle, 30, 150)){
                return Direction.abajo;
            }
            else if(inRange(angle, 0, 30) || inRange(angle, 330, 360)){
                return Direction.izquierda;
            }
            else if(inRange(angle, 210, 330)){
                return Direction.arriba;
            }
            else{
               return Direction.derecha;
           }

        }

        private static boolean inRange(double angle, float init, float end){
            return (angle >= init) && (angle < end);
        }
    }
}