import java.util.Vector;
import java.awt.Graphics2D;
import com.entropyinteractive.Mouse;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.util.List;
import javax.imageio.*;
import java.util.Random;

public class Barracas extends Torre {

    private final String NIVEL1 = "torres/barraca_nivel_01.png";
    private final String NIVEL2 = "torres/barraca_nivel_02.png";
    private final String NIVEL3 = "torres/barraca_nivel_03.png";
    private Vector<Soldado> soldados;
    private Point2D puntoCamino;
    private Point2D puntoCentral;
    private Boolean centro = false;
    private Boolean desposicionado = false;
    private Boolean reposicionar = false;

	public Barracas(PuntoEstrategico p){
		super(p);
        setImagen(NIVEL1);
        venta = 70;
        fuerza = 5;
        alcance = 100;
        frecuencia = 3;
        energia = 50;
        soldados = new Vector<Soldado>();
        areaAlcance = new Ellipse2D.Double(x-imagen.getWidth()-35, y-imagen.getHeight()-20, alcance*2.66, alcance*1.77);
        Random r = new Random();
        int sonido = r.nextInt(11);
        sonido = (sonido > 4) ? 1 : 2;
        Musica.reproducir("barracas_nivel_01_fx0" + (sonido) + ".wav");
        for (int i=0; i<3; i++) {
            soldados.add(new Soldado(this,i+1));
        }
        setAreaRel(+21, +5, -40,-10);
        puntoCentral = new Point2D.Double(x, y);
        try {
            imagenArea = ImageIO.read(getClass().getResource("imagenes/area_barracas.png"));
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

    public void dibujar(Graphics2D g){
        g.drawImage(imagen, x-imagen.getWidth()/2, y-imagen.getHeight()/2, null);
        for (Soldado soldado : soldados){
            soldado.dibujar(g);
        }
    }

    public void mejorar(){
        if (nivelTorre < 3) {
            int monedas = punto.getNivel().getKingdomRush().getMonedas();
            int precio = Precio.get(0,nivelTorre);
            if (monedas >= precio){
                nivelTorre += 1;
                switch(nivelTorre){
                    case 2:
                        setImagen(NIVEL2);
                        fuerza = 9;
                        alcance = 120;
                        frecuencia = 4;
                        break;
                    case 3:
                        setImagen(NIVEL3);
                        fuerza = 13;
                        alcance = 130;
                        frecuencia = 4;
                        break;
                }
                venta += precio;
                Musica.reproducir("construir_fx.wav");
                punto.getNivel().getKingdomRush().setMonedas(-precio);
            }
        }
    }

    public void reiniciarSoldados(){
        for(Soldado soldado : soldados){
            soldado.quitarAtacar();
        }
    }

    public Boolean tienePuntoCamino(){
        return puntoCamino != null;
    }

    public void setPuntoCentral(Point2D puntoCentral){
        this.puntoCentral = puntoCentral;
    }

    public void setPuntoCamino(Camino camino){
        double distancia = 0;
        Point2D temp = new Point2D.Double();
        for (int i=0; i<camino.getCantidadCarriles(); i++){
            List<Point2D> lista = camino.getPuntos(i);
            for (Point2D punto : lista){
                if (distancia == 0){
                    distancia = punto.distance(x, y);
                } else if (distancia > punto.distance(x,y)) {
                    distancia = punto.distance(x,y);
                    temp = punto;
                }
            }
        }
        puntoCamino = temp;
        for(Soldado soldado : soldados){
            soldado.setPuntoCamino(puntoCamino);
        }
    }

    public Point2D getPuntoCentral(){
        return puntoCentral;
    }

    public Boolean movioPuntoCamino(){
        if (puntoCentral.distance(puntoCamino)<5 && !reposicionar) {
            centro = true;
        } else if (puntoCentral.distance(puntoCamino)<5){
            reposicionar = false;
        }
        return centro;
    }

    public void setCentro(){
        centro = true;
    }

    public void moverSoldados(){
        for(Soldado soldado : soldados){
            soldado.posicionarse();
        }
        if (movioPuntoCamino()) {
            for(Soldado soldado : soldados){
                soldado.setMover(false);
            }
        }
    }

    public void atacarEnemigo(Enemigo e){
        for (Soldado soldado : soldados){
            if (!soldado.tieneEnemigo() && !e.tieneSoldado()){
                if (!desposicionado) desposicionado = true;
                soldado.setEnemigo(e);
                e.setSoldado(soldado);
            } else if (!soldado.atacando()) {
                soldado.moverseAlEnemigo();
            } else {
                e.atacar();
                soldado.atacar();
            }
        }
    }

    public int getFuerza(){
        return fuerza;
    }

    public int getEnergia(){
        return energia;
    }

    public Vector<Soldado> getSoldados(){
        return soldados;
    }

    public void reposicionar(){
        centro = false;
        reposicionar = true;
        desposicionado = false;
    }

    public Boolean soldadoAtacando(){
        Boolean res = false;
        for (Soldado soldado : soldados){
            if (soldado.tieneEnemigo()){
                res = true;
            }
        }
        System.out.println(res);
        return res;
    }

    public Boolean getDesposicionado(){
        return desposicionado;
    }
}