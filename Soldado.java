import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Ellipse2D;
import processing.core.PVector;

public class Soldado extends Movil {
	private Barracas barraca;
	private Boolean visible;
	private int fuerza;
	private int energia;
	private int numeroAnimacion = 1;
	private int numeroSoldado;
	private Boolean mover = true;
	private Boolean atacar = false;
	private Point2D puntoCamino;
	private Ellipse2D areaAtaque;
	private Enemigo enemigo;
	private double currTime;
    private double creacion;
	private PVector posicion;
	private PVector aceleracion;
	private PVector velocidad;
	private float maxVelocidad;

	public Soldado(Barracas b, int ns){
		super("animaciones/barraca/mover/1.png",b.getX(), b.getY());
		barraca = b;
		energia = b.getEnergia();
		fuerza = b.getFuerza();
		numeroSoldado = ns;
		b.setPuntoCentral(new Point2D.Double(b.getX(), b.getY()));
		posicion = new PVector(x,y);
    	velocidad = new PVector(0, 0);
	    maxVelocidad = 1;
	    areaAtaque = new Ellipse2D.Double(x-imagen.getWidth()/2+10, y-imagen.getHeight()/2+5, 15, 15);
	    creacion = (double) System.nanoTime() / 1000000000.0;
	}

	public void atacar(){
		if (!tieneEnemigo()) {
			atacar = false;
		}
		if (enemigo.getEnergia() > 0 && tieneEnemigo()) {
			enemigo.detener();
			currTime = (double) System.nanoTime() / 1000000000.0;
			if (currTime - creacion >= 2) {
				creacion = currTime;
				if (enemigo.getEnergia() > 0) {
					enemigo.setEnergia(barraca.getFuerza());
				}
			}
		} else {
			atacar = false;
			enemigo = null;
		}
		System.out.println("atacar");
	}

	public void moverseAlEnemigo() {
		if (tieneEnemigo()) {
			if (!areaAtaque.intersects(enemigo.getX(), enemigo.getY(), enemigo.getAncho(), enemigo.getAlto())) {
				mover = true;
				velocidad = new PVector(0, 0);
				moverAPunto(enemigo.getX(), enemigo.getY());
			} else {
				mover = false;
				enemigo.detener();
				atacar = true;
			}
		} else {
			atacar = false;
		}
	}

	public void dibujar(Graphics2D g){
		if (atacar && tieneEnemigo()) {
			numeroAnimacion = (numeroAnimacion >= 15) ? 1 : numeroAnimacion+1;
			setImagen("animaciones/barraca/atacar/" + String.valueOf(numeroAnimacion) + ".png");
		} else if (mover) {
			numeroAnimacion = (numeroAnimacion >= 6) ? 1 : numeroAnimacion+1;
			setImagen("animaciones/barraca/mover/" + String.valueOf(numeroAnimacion) + ".png");
		}
		else {
			setImagen("animaciones/barraca/mover/1.png");
		}
		g.drawImage(imagen, x - imagen.getWidth()/2, y - imagen.getHeight()/2, null);
	}

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setEnergia(int energia) {
        this.energia = (this.energia - energia < 0) ? 0 : this.energia - energia;
    }

    public int getEnergia() {
        return this.energia;
    }

	public void moverAPunto(int x, int y){
	    PVector vdestino = new PVector((float)x, (float)y);
	    PVector vDireccion = PVector.sub(vdestino, posicion);
	    vDireccion.normalize();
	    vDireccion.mult(0.5f);
	    aceleracion = vDireccion;
	    velocidad.add(aceleracion);
	    velocidad.limit(maxVelocidad);
	    posicion.add(velocidad);
	    this.x = (int)posicion.getX();
	    this.y = (int)posicion.getY();
	    areaAtaque.setFrame(this.x-imagen.getWidth()/2+10, this.y-imagen.getHeight()/2+5, 15, 15);
    }

    public void posicionarse(){
    	PVector vdestino = new PVector((float)puntoCamino.getX(), (float)puntoCamino.getY());
    	switch(numeroSoldado) {
    		case 1:
    			vdestino = new PVector((float)puntoCamino.getX()-20, (float)puntoCamino.getY()-15);
    			break;
    		case 2:
    			vdestino = new PVector((float)puntoCamino.getX()+20, (float)puntoCamino.getY()-15);
    			break;
    		case 3:
    			vdestino = new PVector((float)puntoCamino.getX(), (float)puntoCamino.getY()+15);
    			break;
    	}
	    PVector vDireccion = PVector.sub(vdestino, posicion);
	    vDireccion.normalize();
	    vDireccion.mult(0.5f);
	    aceleracion = vDireccion;
	    velocidad.add(aceleracion);
	    velocidad.limit(maxVelocidad);
	    posicion.add(velocidad);
	    x = (int)posicion.getX();
	    y = (int)posicion.getY();
	    barraca.setPuntoCentral(new Point2D.Double(x,y-15));
	    areaAtaque.setFrame(x-imagen.getWidth()/2+10, y-imagen.getHeight()/2+5, 15, 15);
	    numeroAnimacion = (numeroAnimacion >= 6) ? 1 : numeroAnimacion+1;
		setObjeto("animaciones/barraca/mover/" + String.valueOf(numeroAnimacion) + ".png", x, y);
		if (barraca.getPuntoCentral().distance(puntoCamino)<10){
			barraca.setCentro();
		}
    }

    public void setPuntoCamino(Point2D puntoCamino){
    	this.puntoCamino = puntoCamino;
    }

    public void setMover(Boolean mover){
    	this.mover = mover;
    }

    public Boolean atacando(){
    	return atacar;
    }

    public void setEnemigo(Enemigo e) {
    	enemigo = e;
    }

    public Boolean tieneEnemigo(){
    	return (enemigo != null);
    }

    public void quitarAtacar(){
    	atacar = false;
    	enemigo.quitarDetener();
    	enemigo = null;
    }
}