import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.Graphics2D;
import java.util.List;

public class Ogro extends Enemigo {
	private int numeroAnimacion = 1;

    public Ogro() {
        super("animaciones/ogro/arriba/1.png");
        energia = 120;
        recompensa = 100;
        direccion = "arriba";
        fuerza = 50;
        penalizacion = 3;
        velocidad = 1;
    }

    public void dibujar(Graphics2D g) {
        if (x == 0 && y == 0){} else {
            if (!detenido) {
                numeroAnimacion = (numeroAnimacion == 13) ? 1 : numeroAnimacion+1;
                setObjeto("animaciones/ogro/" + direccion + "/" + String.valueOf(numeroAnimacion) + ".png", x, y);
            } else {
                setObjeto("animaciones/ogro/" + direccion + "/1.png", x, y);
            }
            g.drawImage(imagen, x - imagen.getWidth()/2, y - imagen.getHeight()/2, null);
        }
    }

    public void mover(List<Point2D> puntos) {
        super.mover(puntos);
        area.setFrame(x - imagen.getWidth()/2, y - imagen.getHeight()/2, imagen.getWidth(), imagen.getHeight());
    }

}