public class JugadorKR extends Jugador {
	private int monedas;
	private int vida;

	public JugadorKR(String nombre) {
    	super(nombre);
    	monedas = 300;
    	vida = 20;
	}

    public void setMonedas(int monedas) {
        this.monedas += monedas;
    }

    public int getMonedas() {
        return this.monedas;
    }

    public void setVida(int vida) {
        this.vida = (this.vida - vida < 0) ? 0 : this.vida - vida;
    }

    public int getVida() {
        return this.vida;
    }

    public void reiniciar(){
        vida = 20;
        monedas = 300;
    }
}