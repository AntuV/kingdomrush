import java.awt.*;
import javax.imageio.*;
import com.entropyinteractive.*;
import java.awt.image.*;
import java.awt.geom.Ellipse2D;
import java.util.Random;

public class Mortero extends Torre {

    private final String NIVEL1 = "torres/artilleria_nivel_01.png";
    private final String NIVEL2 = "torres/artilleria_nivel_02.png";
    private final String NIVEL3 = "torres/artilleria_nivel_03.png";
	
	public Mortero(PuntoEstrategico p){
        super(p);
        setImagen(NIVEL1);
        fuerza = 8;
        venta = 120;
        alcance = 90;
        frecuencia = 2;
        areaAlcance = new Ellipse2D.Double(x-imagen.getWidth()-35, y-imagen.getHeight()-20, alcance*2.66, alcance*1.77);
        Random r = new Random();
        int sonido = r.nextInt(11);
        sonido = (sonido > 4) ? 1 : 2;
        Musica.reproducir("mortero_nivel_01_fx0" + (sonido) + ".wav");
        setAreaRel(+21, +5, -40,-10);
        try {
            imagenArea = ImageIO.read(getClass().getResource("imagenes/area_mortero.png"));
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	public void dibujar(Graphics2D g){
        g.drawImage(imagen, x-imagen.getWidth()/2, y-imagen.getHeight()/2, null);
    }

    public void mejorar(){
        if (nivelTorre < 3) {
            int monedas = punto.getNivel().getKingdomRush().getMonedas();
            int precio = Precio.get(0,nivelTorre);
            if (monedas >= precio){
                nivelTorre += 1;
                switch(nivelTorre){
                    case 2:
                        setImagen(NIVEL2);

                        fuerza = 20 ;
                        alcance = 100;
                        frecuencia = 1;

                        break;
                    case 3:
                        setImagen(NIVEL3);
                       
                        fuerza = 55;
                        alcance = 120;
                        frecuencia = 2;
                       
                        break;
                }
                venta += precio;
                Musica.reproducir("construir_fx.wav");
                punto.getNivel().getKingdomRush().setMonedas(-precio);
            }
        }
    }
    public void atacar(){}
}

