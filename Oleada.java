import java.util.Vector;

public class Oleada {
	private int tiempo;
	private long comienzo;
	private Vector<Enemigo> enemigos;

	public Oleada(int tiempo, int goblins, int ogros, int bandidos){
		this.tiempo = tiempo * 1000;
		enemigos = new Vector<Enemigo>();
		for(int i=0;i<goblins;i++){
			enemigos.add(new Goblin());
		}
		for(int i=0;i<ogros;i++){
			enemigos.add(new Ogro());
		}
		for(int i=0;i<bandidos;i++){
			enemigos.add(new Bandido());
		}
		if (goblins == 0 && ogros == 0 && bandidos == 0) {
			enemigos.add(new Yeti());
		}
		this.comienzo = System.currentTimeMillis();
	}

	public Boolean getTiempo(){
		return ((System.currentTimeMillis()-comienzo) >= tiempo);
	}

	public Vector<Enemigo> getEnemigos(){
		return enemigos;
	}
}