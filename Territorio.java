import java.util.Vector;
import java.awt.Graphics2D;
import com.entropyinteractive.Mouse;

public class Territorio {

	private Vector<ObjetoGrafico> objetosGraficos;
	private Vector<Enemigo> enemigos;
	private Vector<Municion> municiones;
	private KingdomRush kingdomRush;
	private Camino camino;
	private long reloj = 0;
	private double currTime = (double) System.nanoTime() / 1000000000.0;
    private double creacion = (double) System.nanoTime() / 1000000000.0;
	private Vector<ObjetoGrafico> recolector = new Vector<ObjetoGrafico>();
    private PuntoEstrategico ptoMenu = null;
    private int flag = 0;
    private Boolean aux = true;
    private Musica musica;
    private Vector<Oleada> oleadas;

	public Territorio(KingdomRush kingdomRush){
		this.kingdomRush = kingdomRush;
        musica = new Musica("musicajuego.wav");
		objetosGraficos = new Vector<ObjetoGrafico>();
		enemigos = new Vector<Enemigo>();
		municiones = new Vector<Municion>();
		camino = kingdomRush.getNivel().getCamino();
        objetosGraficos.add(camino.getMeta().get(0));
		reloj = System.currentTimeMillis();
        oleadas = kingdomRush.getNivel().getOleadas();

		for (PuntoEstrategico p : kingdomRush.getNivel().getPuntosEstrategicos()){
			objetosGraficos.add(p);
		}
	}

	public void actualizar(Mouse m){
        currTime = (double) System.nanoTime() / 1000000000.0;
		if (!municiones.isEmpty()){
			for (ObjetoGrafico o : municiones){
				if(o instanceof Flecha) {
                    ((Flecha)o).mover();
                    if(((Flecha)o).intersecta()){
                        Musica.reproducir("flecha_impacto_fx01.wav");
                        if (((Flecha)o).getEnemigo().getEnergia() == 0) {
                            kingdomRush.setMonedas(((Flecha)o).getEnemigo().getRecompensa());
                            recolector.add(((Flecha)o).getEnemigo());  
                        }
                        recolector.add(o);
                    }
                }
                if(o instanceof Rayo) {
                    ((Rayo)o).mover();
                    if(((Rayo)o).intersecta()){
                        if (((Rayo)o).getEnemigo().getEnergia() == 0) {
                            kingdomRush.setMonedas(((Rayo)o).getEnemigo().getRecompensa());
                            recolector.add(((Rayo)o).getEnemigo());  
                        }
                        recolector.add(o);
                    }
                }
                 if(o instanceof Bomba) {
                    ((Bomba)o).mover();
                    if(((Bomba)o).intersecta()){
                        if (((Bomba)o).getEnemigo().getEnergia() == 0) {
                            kingdomRush.setMonedas(((Bomba)o).getEnemigo().getRecompensa());
                            recolector.add(((Bomba)o).getEnemigo());  
                        }
                        recolector.add(o);
                    }
                }
			}
		}
        for (int i=0; i<oleadas.size(); i++){
            if (oleadas.get(i).getTiempo()){
                Vector<Enemigo> temp = oleadas.get(i).getEnemigos();
                for (Enemigo e : temp){
                    enemigos.add(e);
                    objetosGraficos.add(e);
                }
                oleadas.remove(i);
                aux = true;
            }
        }
        if (oleadas.size() == 0 && enemigos.isEmpty() && kingdomRush.getVida() > 0){
            kingdomRush.victoria();
        } else if (kingdomRush.getVida() == 0){
            kingdomRush.derrota();
        }

        if (System.currentTimeMillis()-reloj > (2 * 1000)) {
            for (int i=0; i<enemigos.size(); i++) {
                if (!((Enemigo)enemigos.get(i)).getVisible()) {
                    Enemigo b = (Enemigo)enemigos.get(i);
                    b.setVisible(true);
                    if (((Enemigo)b) instanceof Yeti){
                        Musica.reproducir("alerta.wav");
                    }
                    break;
                }
            }
            reloj = System.currentTimeMillis();
        }
		for (ObjetoGrafico o : objetosGraficos) {
			if (o instanceof Enemigo) {
                if (((Enemigo)o).getEnergia() == 0) {
                    recolector.add(o);
                }
                if ((currTime - creacion) >= 0.0 && ((Enemigo)o).getVisible()) {
                    creacion = currTime;
                    if (((Enemigo)o) instanceof Yeti) {
                        ((Enemigo)o).getCarril(1);
                    }
                    ((Enemigo)o).mover(camino.getPuntos(((Enemigo)o).getCarril(camino.getCantidadCarriles())));
                    if (((Enemigo)o).intersecta(camino.getMeta())){
                        kingdomRush.setVida(((Enemigo)o).getPenalizacion());
                        Musica.reproducir("descontar_vida_fx01.wav");
                        recolector.add(o);
                    } else if (((Enemigo)o).tieneSoldado()){
                        ((Enemigo)o).atacar();
                    }
                }
            } else if (o instanceof PuntoEstrategico) {
                ((PuntoEstrategico)o).actualizar(m, kingdomRush.getNivel().hayMenu());
                if(((PuntoEstrategico)o).tieneMenu()){
                    ptoMenu = (PuntoEstrategico)o;
                }
                if (!((PuntoEstrategico)o).disponible()){
                    if (((PuntoEstrategico)o).getTorre() instanceof Barracas) {
                        if (!((Barracas)((PuntoEstrategico)o).getTorre()).tienePuntoCamino()){
                            ((Barracas)((PuntoEstrategico)o).getTorre()).setPuntoCamino(camino);
                        } else if (!((Barracas)((PuntoEstrategico)o).getTorre()).movioPuntoCamino()) {
                            ((Barracas)((PuntoEstrategico)o).getTorre()).moverSoldados();
                        }
                    }
                }
                if (!enemigos.isEmpty() && !((PuntoEstrategico)o).disponible()) {
                    for (ObjetoGrafico b : enemigos) {
                        if (((PuntoEstrategico)o).getTorre().colision((Enemigo)b)){
                            if (((Enemigo)b).getEnergia() > 0) {
                                ((PuntoEstrategico)o).getTorre().atacar(municiones, ((Enemigo)b));
                            }
                        }
                    }
                }
            }
		}
		for (ObjetoGrafico a : recolector) {
            if (a instanceof Enemigo) {
                enemigos.remove(a);
            }
            if (a instanceof Municion) {
                municiones.remove(a);
            }
            objetosGraficos.remove(a);
        }
	}

	public void clic(Mouse m){
        for (ObjetoGrafico a: objetosGraficos) {
            if (a instanceof PuntoEstrategico) {
                ((PuntoEstrategico)a).clic(m);
            }
        }
	}

    public void salir(){
        musica.stop();
    }

	public void dibujar(Graphics2D g, Mouse m){
        for (ObjetoGrafico a: objetosGraficos) {
            if (a instanceof Meta)
                ((Meta)a).dibujar(g);
            if (a instanceof Goblin)
                ((Goblin)a).dibujar(g);
            if (a instanceof Ogro)
                ((Ogro)a).dibujar(g);
            if (a instanceof Bandido)
                ((Bandido)a).dibujar(g);
            if (a instanceof Yeti)
                ((Yeti)a).dibujar(g);
            if (a instanceof PuntoEstrategico)
                ((PuntoEstrategico)a).dibujar(g, m);
            if (ptoMenu != null && ptoMenu.getMenu() != null){
                ptoMenu.getMenu().dibujar(g, m);
            }
        }
        if (!municiones.isEmpty()) {
            for (ObjetoGrafico c: municiones) {
                if (c instanceof Flecha) {
                    c.dibujar(g);
                }
               if (c instanceof Bomba) {
                    c.dibujar(g);
                }
                if (c instanceof Rayo) {
                    c.dibujar(g);
                }
            }
        }
	}

    public Musica getMusica(){
        return musica;
    }
}