import com.entropyinteractive.Game;
import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Plataforma extends JFrame implements ActionListener, KeyListener{
	
	private final String arrImagenes[]={"imagenes/juegos/dota2.jpg","imagenes/juegos/leagueoflegends.jpg","imagenes/juegos/kingdomrush.jpg"};
	private final String[] arrWavs1={"plataforma/dota2.wav","plataforma/lol.wav","plataforma/kingdomrush.wav"};
	private Game myGame;
	private Image imagen[] = new Image[3];
	private ImageIcon icoAnterior;
	private ImageIcon icoPlay;
	private ImageIcon icoSiguiente;
	private Imagen imgjuego;
	private int numImagen = 0;
	private JButton botonAnterior;
	private JButton botonPlay;
	private JButton botonSiguiente;
	private JPanel main_panel;
	private Musica musica;
	private String jugador;
	private Thread t;

	public Plataforma(){
		super("Plataforma de Videojuegos");
		musica = new Musica("plataforma/dota2.wav");
		setSize(700,600);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		for(int i=0; i<arrImagenes.length ; i++)
			imagen[i] =  Toolkit.getDefaultToolkit().getImage(arrImagenes[i]);
		imgjuego  = new Imagen(imagen[0]);
		imgjuego.addKeyListener(this);
		icoAnterior = new ImageIcon("imagenes/icoAnterior.png");
		botonAnterior = new JButton(icoAnterior);
		botonAnterior.addActionListener(this);
		botonAnterior.setOpaque(false);
		botonAnterior.setContentAreaFilled(false);
		botonAnterior.setBorderPainted(false);
		botonAnterior.setBorder(new EmptyBorder(0,0,0,0));
		botonAnterior.addMouseListener(new MouseAdapter() {
		    public void mouseEntered(MouseEvent evt) {
		        	Musica.reproducir("plataformahit.wav");
			}
			public void mouseClicked(MouseEvent evt) {
		        	Musica.reproducir("plataformaclic.wav");
			}
		});
		botonAnterior.addKeyListener(this);

		icoSiguiente = new ImageIcon("imagenes/icoSiguiente.png");
		botonSiguiente = new JButton(icoSiguiente);
		botonSiguiente.addActionListener(this);
		botonSiguiente.setOpaque(false);
		botonSiguiente.setContentAreaFilled(false);
		botonSiguiente.setBorderPainted(false);
		botonSiguiente.setBorder(new EmptyBorder(0,0,0,0));
		botonSiguiente.addMouseListener(new MouseAdapter() {
		    public void mouseEntered(MouseEvent evt) {
		        	Musica.reproducir("plataformahit.wav");
			}
			public void mouseClicked(MouseEvent evt) {
		        	Musica.reproducir("plataformaclic.wav");
			}
		});		
		botonSiguiente.addKeyListener(this);

		icoPlay = new ImageIcon("imagenes/icoPlay.png");
		botonPlay = new JButton(icoPlay);
		botonPlay.addActionListener(this);
		botonPlay.setOpaque(false);
		botonPlay.setContentAreaFilled(false);
		botonPlay.setBorderPainted(false);
		botonPlay.setBorder(new EmptyBorder(0,0,0,0));
		botonPlay.addMouseListener(new MouseAdapter() {
		    public void mouseEntered(MouseEvent evt) {
		        	Musica.reproducir("plataformahit.wav");
			}
			public void mouseClicked(MouseEvent evt) {
		        	Musica.reproducir("plataformaclic.wav");
			}
		});	
		botonPlay.addKeyListener(this);

		main_panel = new JPanel();		

		main_panel.setLayout(new BorderLayout());		
		main_panel.add(botonAnterior, BorderLayout.WEST);
		main_panel.add(imgjuego, BorderLayout.CENTER);
		main_panel.add(botonSiguiente, BorderLayout.EAST);
		main_panel.add(botonPlay, BorderLayout.SOUTH);
		add(main_panel);
		NombreJugador.getJugador(this);
	}

	public static void main(String[] args) {
		Plataforma plataforma = new Plataforma();
	}

	public void actionPerformed(ActionEvent e) {
		if ((e.getSource().equals(botonAnterior)) || (e.getSource().equals(botonSiguiente))){
			if(e.getSource().equals(botonAnterior)){
				if (numImagen == 0) 
					{
						numImagen = 2;
					}
				else {numImagen -= 1;}
				imgjuego.cambiarImagen(imagen[numImagen]);
			}
			if(e.getSource().equals(botonSiguiente)){
				if (numImagen == 2) 
					{	
						numImagen = 0;
					}
				else {numImagen += 1;}
				imgjuego.cambiarImagen(imagen[numImagen]);
			}
			musica.cambiarMusica(arrWavs1[numImagen]);
		}
		if(e.getSource().equals(botonPlay) && numImagen == 2){
			myGame = new KingdomRush(this);
			this.setVisible(false);
			musica.stop();
            t = new Thread() {
                public void run() {
                	try {
                    	myGame.run(1.0 / 30.0);
                	} catch (IllegalStateException ex) {
					} catch (Exception e) {
						e.printStackTrace();
						myGame.shutdown();
					}
                }
            };
            t.start();
		}
	}

	@Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode()==KeyEvent.VK_LEFT){
        	if (numImagen == 0) 
				{
					numImagen = 2;
				}
			else {numImagen -= 1;}
			imgjuego.cambiarImagen(imagen[numImagen]);
        }
        if (e.getKeyCode()==KeyEvent.VK_RIGHT){
			if (numImagen == 2) 
				{	
					numImagen = 0;
				}
			else {numImagen += 1;}
			imgjuego.cambiarImagen(imagen[numImagen]);
        }
        if (e.getKeyCode()==KeyEvent.VK_ENTER && numImagen == 2){
			myGame = new KingdomRush(this);
			this.setVisible(false);
			musica.stop();
            t = new Thread() {
                public void run() {
                	try {
                    	myGame.run(1.0 / 30.0);
                	} catch (IllegalStateException ex) {
					} catch (Exception e) {
						e.printStackTrace();
						myGame.shutdown();
					}
                }
            };
            t.start();
        }
        musica.cambiarMusica(arrWavs1[numImagen]);
    }

    @Override
    public void keyReleased(KeyEvent arg0) {}

    @Override
    public void keyTyped(KeyEvent arg0) {}

	public void musica(){
		musica.start(true);
	}

	public void itemStateChanged(ItemEvent i) {}

	public void setJugador(String j){
		jugador = j;
		setVisible(true);
		imgjuego.requestFocusInWindow();
	}

	public String getJugador(){
		return jugador;
	}
}

class Imagen extends Canvas {

	private static final long serialVersionUID = 7526472295622776147L;
	private Image imagen;

	public Imagen(Image i) {
    	imagen = i;
    	this.setPreferredSize(new Dimension(500,500));
  	}

	public void cambiarImagen(Image i){
		imagen = i;
		repaint();
	}
	
	@Override
	public void paint(Graphics g) {
	    g.setColor(Color.black);
	    g.fillRect(0,0,500,500);
	    g.drawRect(0,0,500+1 ,500+1);
	    g.drawImage(imagen,0,0,this);
	}
}