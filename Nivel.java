import java.io.*;
import java.util.Vector;
import java.io.*;
import java.awt.image.*;
import javax.imageio.*;
import com.entropyinteractive.* ;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

public class Nivel {

	private KingdomRush kr;
	private ObjetoGrafico fondo;
	private Vector<Oleada> oleadas; // (tiempo, goblins, bandidos, orcos)
	private int cant_pto_estr;
	private int numero_nivel;
	private int monedas_inicio;
	private Vector<PuntoEstrategico> puntos;
	private Camino camino;
	private PuntoEstrategico ptoMenu = null;
	private Boolean hayMenu;

	public Nivel(int nivel, KingdomRush kr){
		this.kr = kr;
		numero_nivel = nivel;
		String rutaNivel = "mapas/nv" + nivel + "-config.txt";
		oleadas = new Vector<Oleada>();
		String res = null;
		String[] readOleada;
		int[] oleadaTemp;
		try {
			RandomAccessFile config = new RandomAccessFile(rutaNivel,"r");
			cant_pto_estr = Integer.valueOf(config.readLine());
			for (int i=0; i<6; i++){
				res = config.readLine();
				readOleada = res.split(",");
				oleadas.add(new Oleada(Integer.valueOf(readOleada[0]),Integer.valueOf(readOleada[1]),Integer.valueOf(readOleada[2]),Integer.valueOf(readOleada[3])));
			}
			if (numero_nivel == 2) {
				oleadas.add(new Oleada(210,0,0,0));
			}
		} catch(Exception e){
			e.printStackTrace();
		}

		camino = new Camino(nivel);

		String[] ptoActual = new String[2];
		puntos = new Vector<PuntoEstrategico>(cant_pto_estr);
		String rutaPtos = "mapas/nv" + nivel + "-ptosestr.txt";
		try {
	        RandomAccessFile cord = new RandomAccessFile(rutaPtos,"r");
	        res = null;
	        do {
	        	res = cord.readLine();
	        	if (res != null) {
	        		ptoActual = res.split(",");
	        		puntos.add(new PuntoEstrategico(Integer.valueOf(ptoActual[0]), Integer.valueOf(ptoActual[1]), this));
	        	}
	        } while(res != null);
		} catch(Exception e){
			e.printStackTrace();
		}

		hayMenu = false;
	}

	public void actualizar(Mouse m){
		for (PuntoEstrategico p : puntos) {
			p.actualizar(m, hayMenu);
			if(p.tieneMenu()){
				ptoMenu = p;
			}
		}
	}

	public void clic(Mouse m){
		for (PuntoEstrategico p : puntos) {
			p.clic(m);
		}
	}

	public void dibujar(Graphics2D g, Mouse m){
		fondo.dibujar(g);
		for (PuntoEstrategico punto : puntos) {
			punto.dibujar(g, m);
		}
	 	if (ptoMenu != null && ptoMenu.getMenu() != null){ //maña para que los ptos no superpongan al menu
	 		ptoMenu.getMenu().dibujar(g, m);
		}
	}

	public Camino getCamino(){
		return camino;
	}

	public Boolean hayMenu(){
		return hayMenu;
	}

	public void setMenu(Boolean b) {
		hayMenu = b;
	}

	public Vector<PuntoEstrategico> getPuntosEstrategicos(){
		return this.puntos;
	}

	public KingdomRush getKingdomRush() {
		return this.kr;
	}

	public Vector<Oleada> getOleadas() {
		return oleadas;
	}
}