import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.RandomAccessFile;
import java.io.PrintWriter;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;

class NombreJugador extends JFrame implements KeyListener{
	private JTextField nombre;
	private JPanel panel;
	private Font toonish;
	private Plataforma p;
	private KingdomRush kr;
	private RandomAccessFile archivoUltimoJugador;
	private String ultimoJugador;

	public void generarInterfaz(){
		try {
			archivoUltimoJugador = new RandomAccessFile("ultimojugador.txt","r");  
	        ultimoJugador = null;
	       	ultimoJugador = archivoUltimoJugador.readLine();
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel"); 
			toonish = Font.createFont(Font.TRUETYPE_FONT, new File("imagenes/TOONISH.ttf")).deriveFont(20f);
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("imagenes/TOONISH.ttf")));
		} catch (Exception e){}
		setSize(300,100);
		setLocationRelativeTo(null);
		setResizable(false);
		setUndecorated(true);
		setLayout(new BorderLayout());
		JLabel background = new JLabel(new ImageIcon(getClass().getResource("imagenes/plataforma/jugador.png")));
		add(background);
		background.setLayout(new BorderLayout());
		nombre = new JTextField(ultimoJugador, 100);
		nombre.setOpaque(false);
		nombre.setHorizontalAlignment(JTextField.CENTER);
		nombre.setFont(toonish);
		nombre.setForeground(Color.YELLOW);
		nombre.addKeyListener(this);
		background.add(nombre, BorderLayout.CENTER);
		nombre.requestFocusInWindow();
		setVisible(true);
	}

	public static void getJugador(Plataforma p){
		NombreJugador j = new NombreJugador(p);

	}

	public static void cambiarJugador(KingdomRush kr){
		NombreJugador j = new NombreJugador(kr);

	}

	public NombreJugador(Plataforma p){
		super("Jugador");
		this.p = p;
		generarInterfaz();
	}

	public NombreJugador(KingdomRush kr){
		super("Jugador");
		this.kr = kr;
		generarInterfaz();
	}

    public void actionPerformed(ActionEvent submitClicked) {}

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode()==KeyEvent.VK_ENTER){
        	if (nombre.getText().length() < 3) {

        	} else {
        		if (p != null) {
	        		p.setJugador(nombre.getText().toLowerCase());
        		} else {
        			kr.setJugador(nombre.getText().toLowerCase());
        		}
	        	try{
					PrintWriter writer = new PrintWriter("ultimojugador.txt", "UTF-8");
					writer.println(nombre.getText().toLowerCase());
					writer.close();
				} catch (Exception ex) {}
	            this.setVisible(false);
	        }
        } else if (e.getKeyCode()==KeyEvent.VK_BACK_SPACE || e.getKeyCode()==KeyEvent.VK_DELETE){
        	if (nombre.getText().toLowerCase().equals(ultimoJugador)) {
	        	nombre.setText("");
	        }
        } else {
        	if (nombre.getText().length() >= 10) {
        		nombre.setText(nombre.getText().substring(0,9));
        	}
        }
    }

    @Override
    public void keyReleased(KeyEvent arg0) {}

    @Override
    public void keyTyped(KeyEvent arg0) {}
}