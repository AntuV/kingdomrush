import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.Graphics2D;
import java.util.List;

public class Yeti extends Enemigo {

    private int numeroAnimacion = 1;

    public Yeti() {
        super("animaciones/yeti/abajo/1.png");
        energia = 500;
        recompensa = 0;
        direccion = "abajo";
        fuerza = 75;
        penalizacion = 20;
        velocidad = 1;
    }

    public void dibujar(Graphics2D g) {
        if (x == 0 && y == 0){} else {
            numeroAnimacion = (numeroAnimacion == 13) ? 1 : numeroAnimacion+1;
            setObjeto("animaciones/yeti/" + ((direccion == "arriba") ? "abajo" : direccion)  + "/" + String.valueOf(numeroAnimacion) + ".png", x, y);
            g.drawImage(imagen, x - imagen.getWidth()/2, y - imagen.getHeight()/2-50, null);
        }
        System.out.println(x + " " + y);
    }

    public void mover(List<Point2D> puntos) {
        super.mover(puntos);
        area.setFrame(x - imagen.getWidth()/2, y - imagen.getHeight()/2, imagen.getWidth(), imagen.getHeight());
    }

}
