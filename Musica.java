import javax.sound.sampled.*;
import java.io.InputStream;

public class Musica {
    Clip clip = null;
    float volumen;

    public Musica(String ruta) {
        volumen = 0.15f;
        FloatControl gainControl;
        try {
            InputStream is = getClass().getResourceAsStream("sonido/" + ruta);
            AudioInputStream inputStream = AudioSystem.getAudioInputStream(is);
            DataLine.Info info = new DataLine.Info(Clip.class, inputStream.getFormat());
            clip = (Clip)AudioSystem.getLine(info);
            clip.open(inputStream);
        } catch (Exception ex) {
            System.out.println(ex);
        }
        gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);        
        gainControl.setValue(20f * (float) Math.log10(volumen));
        clip.setFramePosition(0);
        clip.start();
        clip.loop(Clip.LOOP_CONTINUOUSLY);
    }

    public void stop(){
        clip.stop();
    }

    public void start(Boolean reiniciar){
        if (reiniciar) clip.setFramePosition(0);
        clip.start();
        clip.loop(Clip.LOOP_CONTINUOUSLY);
    }

    public void alternar(){
        if (clip.isRunning()){
            stop();
        } else {
            start(false);
        }
    }

    public void setVolumen(int vol){ //vol es del 0 al 100
        volumen = (float) vol / 100;
        FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);   
        gainControl.setValue(20f * (float) Math.log10(volumen));
    }

    public void cambiarMusica(String ruta) {
        clip.stop();
        try {
            InputStream is = getClass().getResourceAsStream("sonido/" + ruta);
            AudioInputStream inputStream = AudioSystem.getAudioInputStream(is);
            DataLine.Info info = new DataLine.Info(Clip.class, inputStream.getFormat());
            clip = (Clip)AudioSystem.getLine(info);
            clip.open(inputStream);
        } catch (Exception ex) {}
        setVolumen(30);
        clip.setFramePosition(0);
        clip.start();
        clip.loop(Clip.LOOP_CONTINUOUSLY);
    }

    public static void reproducir(String nombre){
        Clip clip;
        FloatControl gainControl;
        try {
            InputStream is = Musica.class.getClassLoader().getResourceAsStream("sonido/" + nombre);
            AudioInputStream inputStream = AudioSystem.getAudioInputStream(is);
            DataLine.Info info = new DataLine.Info(Clip.class, inputStream.getFormat());
            clip = (Clip)AudioSystem.getLine(info);
            clip.open(inputStream);
            gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);        
            gainControl.setValue(20f * (float) Math.log10(0.3f));
            clip.setFramePosition(0);
            clip.start();
        } catch (Exception ex) {}
    }
}