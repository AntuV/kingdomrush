import java.util.Vector;
import java.awt.geom.Point2D;
import java.awt.Graphics2D;

public class Meta extends ObjetoGrafico {

    public Meta (Point2D punto) {
    	super((int)punto.getX(), (int)punto.getY());
    }

    public Point2D getPunto() {
        return new Point2D.Double(x, y);
    }

    public void dibujar(Graphics2D g){
		g.drawImage(imagen, this.getX()-imagen.getWidth()/2, this.getY()-imagen.getHeight()/2, null);
    }
}