import java.awt.Shape;
import java.util.Vector;
import java.util.List;
import java.util.ArrayList;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.io.*;

public class Camino {

		private Vector<List<Point2D>> puntos = new Vector<List<Point2D>>(); 
		private Vector<Path2D> carril = new Vector<Path2D>();
		private Vector<Meta> metas = new Vector<Meta>();
		public Camino(int nivel){		
			String[] ptoPathActual = new String[6]; 	
			Vector<String> cordPath = new Vector<String>(); 
			try {
		        String caminoNivel ="mapas/nv" + nivel + "-path.txt";
				RandomAccessFile archcamino = new RandomAccessFile(caminoNivel,"r");  
		        String res = null;
		        do {
		        	res = archcamino.readLine();
		        	cordPath.add(res); 
		        } while(res != null); 	
			} catch(Exception e){
				e.printStackTrace();
			}
			cordPath.remove(cordPath.size()-1);
			Path2D p = new Path2D.Double();
			for(int i=0;i<cordPath.size();i++){
				String res = cordPath.get(i); 
				if (res.length() < 3) {
					carril.add(p);
					p = new Path2D.Double();
				} else {
					ptoPathActual = res.split(",");
					if (ptoPathActual.length == 2) {
						p.moveTo(Integer.valueOf(ptoPathActual[0]),Integer.valueOf(ptoPathActual[1]));
					} else {
						p.curveTo(Integer.valueOf(ptoPathActual[0]),Integer.valueOf(ptoPathActual[1]),Integer.valueOf(ptoPathActual[2]),Integer.valueOf(ptoPathActual[3]),Integer.valueOf(ptoPathActual[4]),Integer.valueOf(ptoPathActual[5]));
					}
				}
			}
			for(int i=0; i<carril.size(); i++) {
				List<Point2D> ptemp = new ArrayList<>(50);
				PathIterator pi = carril.get(i).getPathIterator(null, 0.0);
				pi.next();
				double[] cursor = new double[2];
				cursor[0]=0; cursor[1]=0;
				double[] coords = null;
				while (!pi.isDone()) {
					coords = new double[6];
					pi.currentSegment(coords);
					while (!pi.isDone() && Point2D.distance(coords[0],coords[1],cursor[0],cursor[1]) < 2){
						pi.next();
						if (!pi.isDone()){
							pi.currentSegment(coords);
						}
					}
					ptemp.add(new Point2D.Double(coords[0], coords[1]));
					cursor[0] = coords[0];
					cursor[1] = coords[1];	
		            pi.next();
				}
				puntos.add(ptemp);
				metas.add(new Meta(new Point2D.Double(coords[0],coords[1])));
			}
			if(nivel == 1) {
				metas.get(0).setImagen("meta1.png");
			} else {
				metas.get(0).setImagen("meta2.png");
			}
		}

		public List<Point2D> getPuntos(int carril){
			return puntos.get(carril);
		}

		public int getCantidadCarriles(){
			return puntos.size();
		}

		public Path2D getCarril(int carril){
			return this.carril.get(carril-1);
		}

		public Vector<Meta> getMeta(){
			return metas;
		}
}