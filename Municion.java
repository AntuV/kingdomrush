import processing.core.PVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.Graphics2D;
import java.awt.Color;

public class Municion extends Movil {

	  private PVector posicion;
  	private PVector velocidad;
  	private PVector aceleracion;
  	private float maxVelocidad;
  	private float xoff, yoff;
  	private float r = 16;
    private Enemigo e;
    private int fuerza;

    public Municion(int x, int y, Enemigo e, int fuerza) {
    	super(x, y);
      this.e = e;
      this.fuerza = fuerza;
    	posicion = new PVector(x,y+30);
    	velocidad = new PVector(0, 0);
	    maxVelocidad = 4;
	    xoff = 1000;
	    yoff = 0;
	    area = new Rectangle2D.Double(posicion.x, posicion.y, 13,4);
    }

  	public void mover(){
  		  PVector vdestino = new PVector(e.getX(), e.getY());
		    PVector vDireccion = PVector.sub(vdestino, posicion);
		    vDireccion.normalize();
		    vDireccion.mult(0.5f);
		    aceleracion = vDireccion;
		    velocidad.add(aceleracion);
		    velocidad.limit(maxVelocidad);
		    posicion.add(velocidad);
		    area = new Rectangle2D.Double(posicion.x, posicion.y,13,4);
        if (area.intersects(e.getX(),e.getY(),35,30)){
          e.setEnergia(fuerza);
        }
  	}

  	public void dibujar(Graphics2D g){
  		double theta = Math.atan2((double)velocidad.y,(double)velocidad.x);
		  AffineTransform affineT=g.getTransform();// almaceno la AffineTransform antes de hacer las operaciones
  		g.translate((int)posicion.x, (int)posicion.y);
  		g.rotate(theta);

		if (imagen!=null){
				g.drawImage(imagen,0,0,null);
		}else{
			g.setColor(Color.blue);
			g.fillRect(0,0,(int)area.getWidth() ,(int)area.getHeight());
		}

  		 g.translate(-(int)posicion.x, -(int)posicion.y);
  		 g.setTransform(affineT);// restauro la AffineTransform
  	}

  	public boolean intersecta(){
  		return area.intersects(e.getX(),e.getY(),e.getAncho(),e.getAlto());
  	}

    public Enemigo getEnemigo(){
      return e;
    }
}