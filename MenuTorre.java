import java.util.Vector;
import java.awt.*;
import javax.imageio.*;
import com.entropyinteractive.*;
import java.awt.image.*;

public class MenuTorre extends ObjetoGrafico {
	private Vector<IconoMenu> iconos = new Vector<IconoMenu>();
	private PuntoEstrategico punto;
	public MenuTorre(PuntoEstrategico p){
		super("circulo_iconos.png",p.getX(),p.getY());
		punto = p;
		iconos.add(new IconoMenu(0, p));
		iconos.add(new IconoMenu(1, p));
		iconos.add(new IconoMenu(2, p));
		iconos.add(new IconoMenu(3, p));
		iconos.add(new IconoMenu(4, p));
		iconos.add(new IconoMenu(5, p));
	}

	public void dibujar(Graphics2D g, Mouse m){
		if (punto.tieneMenu()) {
			g.drawImage(imagen,x-imagen.getWidth()/2,y-imagen.getHeight()/2,null);
			if (punto.disponible()) {
				for (int i=0; i<4; i++) {
					iconos.get(i).dibujar(g,m);
				}
			} else {
					if (punto.getTorre().getNivelTorre() < 3) {
						int monedas = punto.getNivel().getKingdomRush().getMonedas();
						int tipo = punto.getTorre().getTipo();
						int precio = Precio.get(tipo,punto.getTorre().getNivelTorre());
			            if (monedas >= precio){
			            	iconos.get(4).dibujar(g,m);
			            }
			        }
					iconos.get(5).dibujar(g,m);
			}
		}
	}

	public void clic(Mouse m){
		if (contiene(m)) {
			if (punto.disponible()) {
				for (int i=0; i<4; i++) {
					iconos.get(i).clic(m);
				}
			} else {
				for (int i=4; i<6; i++) {
					iconos.get(i).clic(m);
				}
			}
		}
	}
}