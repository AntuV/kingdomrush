import java.awt.Color;
import java.util.List;
import java.util.ArrayList;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Point2D;

public class Bomba extends Municion {
	public Bomba(int x, int y, Enemigo e, int fuerza){
		super(x, y, e, fuerza);
		setImagen("municion/Bomba/bomba.png");
	}
}