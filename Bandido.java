import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.Graphics2D;
import java.util.List;

public class Bandido extends Enemigo {

	private int numeroAnimacion = 1;

    public Bandido() {
        super("animaciones/bandidos/arriba/1.png");
        energia = 90;
        recompensa = 60;
        direccion = "arriba";
        penalizacion = 1;
        fuerza = 25;
        velocidad = 2;
    }

    public void dibujar(Graphics2D g) {
        if (x == 0 && y == 0){} else {
        	numeroAnimacion = (numeroAnimacion == 12) ? 1 : numeroAnimacion+1;
        	setObjeto("animaciones/bandidos/" + direccion + "/" + String.valueOf(numeroAnimacion) + ".png", x, y);
        	g.drawImage(imagen, x - imagen.getWidth()/2, y - imagen.getHeight()/2, null);
        }
    }

    public void mover(List<Point2D> puntos) {
        super.mover(puntos);
        area.setFrame(x - imagen.getWidth()/2, y - imagen.getHeight()/2, imagen.getWidth(), imagen.getHeight());
    }

}
