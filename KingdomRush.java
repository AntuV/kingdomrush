import com.entropyinteractive.*;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.Font;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.awt.RenderingHints;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Vector;
import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.UIManager;

public class KingdomRush extends Game {

    private Plataforma plataforma;
    private Boolean nivel = false;
    private Boolean derrota = false;
    private Boolean mapa = false;
    private Boolean pausa = false;
    private Boolean victoria = false;
    private Boolean volver = false;
    private Boton boton_continuar;
    private Boton boton_creditos;
    private Boton boton_iniciar;
    private Boton boton_nombre;
    private Boton boton_opciones;
    private Boton boton_pausa;
    private Boton boton_reiniciar;
    private Boton boton_salir;
    private Boton boton_volver;
    private Font toonish;
    private int numeroNivel = 0;
    private JugadorKR jugador;
    private Musica musica;
    private Nivel miNivel;
    private ObjetoGrafico fondo;
    private ObjetoGrafico hud;
    private ObjetoGrafico imagen_derrota;
    private ObjetoGrafico imagen_victoria;
    private ObjetoGrafico negro;
    private Territorio territorio;
    private Vector<Boton> banderas;

    public KingdomRush(Plataforma j) {
        super("KingdomRush", 700, 600);
        jugador = new JugadorKR(j.getJugador());
        plataforma = j;
    }

    public KingdomRush() {
        super("KingdomRush", 700, 600);
    }

    public void gameStartup() {
    	banderas = new Vector<Boton>(2);
    	banderas.add(new Boton("menu/bandera.png", 102, 463, "menu/bandera1.png"));
        banderas.add(new Boton("menu/bandera.png", 87, 396, "menu/bandera1.png"));
    	boton_iniciar = new Boton("menu/boton_iniciar.png",175, 450,"menu/boton_iniciar1.png");
    	boton_volver = new Boton("menu/boton_volver.png", 79, 550, "menu/boton_volver1.png");
    	boton_salir = new Boton("menu/boton_salir.png", 49, 562, "menu/boton_salir1.png");
    	boton_pausa = new Boton("boton_pausa_01.png", 678, 18, "boton_pausa_02.png");
		boton_opciones = new Boton("menu/boton_opciones.png",178,523,"menu/boton_opciones1.png");
    	boton_creditos = new Boton("menu/boton_creditos.png",175,571,"menu/boton_creditos1.png");
    	fondo = new ObjetoGrafico("menu/fondo.jpg",0,0);
    	musica = new Musica("plataforma/kingdomrush.wav");
    	negro = new ObjetoGrafico("negro.png",0,0);
        hud = new ObjetoGrafico("hud.png",10,10);
        imagen_derrota = new ObjetoGrafico("derrota.png",71,0);
        imagen_victoria = new ObjetoGrafico("victoria.png",166,43);
        boton_reiniciar = new Boton("reiniciar.png",350, 280, "reiniciar1.png");
        boton_nombre = new Boton("boton.png",600, 565, "boton1.png");
        boton_continuar = new Boton("continuar.png", 350, 380, "continuar1.png");
    	try {
	    	UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel"); 
			toonish = Font.createFont(Font.TRUETYPE_FONT, new File("imagenes/TOONISH.ttf")).deriveFont(20f);
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("imagenes/TOONISH.ttf")));
		} catch (Exception exce) {}
		if (jugador == null) {
            jugador = new JugadorKR("AAAAAAAAAA");
		}
    }


    public void gameUpdate(double delta) {
        Mouse mouse = getMouse();
        LinkedList<MouseEvent> mouseEvents = mouse.getEvents();
        if(mouse.isButtonClicked()) {
        	Musica.reproducir("mouseclic.wav");
            if (mapa==false) {
                if (nivel==true) {
                	if (!pausa) {
                        territorio.clic(mouse);
    					if (boton_pausa.contiene(mouse)){
					       if (!pausa) pausa = true;
					    }   
                	} else if (boton_salir.contiene(mouse)){
                		pausa = false;
                		mapa = true;
                        volver = true;
                        fondo.setImagen("fondoPrevioNivel.png");
                        territorio.salir();
                        territorio = null;
                        miNivel = null;
                        numeroNivel = 0;
                        nivel = false;
                        musica.start(true);
                        boton_salir = new Boton("menu/boton_salir.png", 49, 562, "menu/boton_salir1.png");
                        jugador.reiniciar();
                        derrota = false;
                        victoria = false;
                	} else if (boton_reiniciar.contiene(mouse)) {
                        territorio.salir();
                        jugador.reiniciar();
                        miNivel = new Nivel(numeroNivel, this);
                        territorio = new Territorio(this);
                        nivel = true;
                        mapa = false;
                        derrota = false;
                        victoria = false;
                        pausa = false;
                        boton_salir = new Boton("salir.png", 350, 350, "salir1.png");
                    } else if (boton_continuar.contiene(mouse) && numeroNivel == 1) {
                        territorio.salir();
                        jugador.reiniciar();
                        numeroNivel++;
                        miNivel = new Nivel(numeroNivel, this);
                        territorio = new Territorio(this);
                        fondo.setImagen("mapas/nivel" + numeroNivel + ".png");
                        nivel = true;
                        mapa = false;
                        derrota = false;
                        victoria = false;
                        pausa = false;
                        boton_salir = new Boton("salir.png", 350, 350, "salir1.png");
                    }
                     else {
                		pausa = false;
                	}
                } else {

                    if (boton_iniciar.contiene(mouse)) {
                        mapa = true;
                        volver = true;
                        fondo.setImagen("fondoPrevioNivel.png");
                    } else if (boton_salir.contiene(mouse)) {
                        this.stop();
                    } else if (boton_nombre.contiene(mouse)) {
                        NombreJugador.cambiarJugador(this);
                    }
                }
            } else {

                if (nivel==false) {
                	for (int i=0; i<banderas.size(); i++){
                		if (banderas.get(i).contiene(mouse)){
                			musica.stop();
                			miNivel = new Nivel(i+1, this);
                			numeroNivel = i+1;
                			territorio = new Territorio(this);
                			fondo.setImagen("mapas/nivel" + numeroNivel + ".png");
                			nivel = true;
                        	mapa = false;
                        	boton_salir = new Boton("salir.png", 350, 350, "salir1.png");
                		}
                	}
                    if (volver==true && (boton_volver.contiene(mouse))) {
                    	fondo.setImagen("menu/fondo.jpg");
                        mapa = false;
                    }
                }
            }
        }
        Keyboard keyboard = this.getKeyboard();
        LinkedList < KeyEvent > keyEvents = keyboard.getEvents();
        for (KeyEvent event : keyEvents) {
            if ((event.getID() == KeyEvent.KEY_PRESSED) && (event.getKeyCode() == KeyEvent.VK_ESCAPE)) {
            	Musica.reproducir("mouseclic.wav");
            	if (nivel) {
            		if (!pausa) pausa = true; else pausa = false;
            	} else if (mapa) {
            		fondo.setImagen("menu/fondo.jpg");
            		mapa = false;
            	}
            }
        }

        if (nivel==true && !pausa) {
            territorio.actualizar(mouse);
        }

    }


    public void gameDraw(Graphics2D g) {
    	Mouse m = getMouse();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        fondo.dibujar(g);
        g.setFont(toonish);
        if (mapa) {
        	boton_volver.dibujar(g, m);
        	for (Boton bandera : banderas){
        		bandera.dibujar(g, m);
        	}
        } else if (!mapa && nivel) {
            territorio.dibujar(g, m);
            g.setColor(Color.WHITE);
            hud.dibujar(g);
            g.drawString(String.valueOf(jugador.getVida()),55,36);
            g.drawString(String.valueOf(jugador.getMonedas()),110,36);
            g.setColor(Color.YELLOW);
            g.setFont(toonish.deriveFont(15f));
            g.drawString(jugador.getNombre(),90-((int)(4.5*jugador.getNombre().length())),58);
            boton_pausa.dibujar(g, m);
        } else {
            boton_nombre.dibujar(g, m);
            g.setColor(Color.YELLOW);
    		g.drawString(jugador.getNombre(),590-((int)(4.5*jugador.getNombre().length())),572);
        	boton_iniciar.dibujar(g, m);
        	boton_opciones.dibujar(g, m);
        	boton_creditos.dibujar(g, m);
        	boton_salir.dibujar(g, m);
        }
        if(pausa){
        	negro.dibujar(g);
            if (derrota) {
                imagen_derrota.dibujar(g);
            } else if (victoria) {
                imagen_victoria.dibujar(g);
                if (numeroNivel == 1) {
                    boton_continuar.dibujar(g, m);
                }
            }
            boton_reiniciar.dibujar(g,m);
        	boton_salir.dibujar(g, m);
        }
    }

    public void gameShutdown() {
    	musica.stop();
    	if(territorio != null) territorio.getMusica().stop();
    	this.stop();
    	if (plataforma != null){
    		plataforma.setVisible(true);
    		plataforma.musica();
    	}
    }


    public Nivel getNivel() {
        return miNivel;
    }

    public static void main(String[] args) {
        Game myGame = new KingdomRush();
        Thread t = new Thread() {
            public void run() {
            	try {
                	myGame.run(1.0 / 30.0);
            	} catch (IllegalStateException ex) {
				} catch (Exception e) {
					e.printStackTrace();
				}
            }
        };
        t.start();
    }

    public int getNumeroNivel(){
    	return numeroNivel;
    }

    public void setMonedas(int monedas){
        jugador.setMonedas(monedas);
    }

    public int getMonedas(){
        return jugador.getMonedas();
    }

    public int getVida(){
        return jugador.getVida();
    }

    public void setVida(int vida){
        jugador.setVida(vida);
    }

    public void derrota(){
        pausa = true;
        derrota = true;
        boton_salir.mover(350, 380);
        boton_reiniciar.mover(350, 255);
    }

    public void victoria(){
        pausa = true;
        victoria = true;
        boton_salir.mover(350, 520);
        boton_reiniciar.mover(350, 450);
        Musica.reproducir("victoria.wav");
    }

    public void setJugador(String alias) {
        jugador = new JugadorKR(alias);
    }

}
