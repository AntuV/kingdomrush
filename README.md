![kr-logo.png](https://bitbucket.org/repo/64ze7EG/images/2555919985-kr-logo.png)

## **Proyecto Integrador de Programación Orientada a Objetos 2017** ##

# Kingdom Rush #

Se debe construir una plataforma de software que permita ejecutar diferentes
video-juegos. La plataforma deberá implementar los elementos básicos comunes a todos los video-juegos de forma que permita desarrollar fácilmente nuevos juegos y agregarlos a la misma.    

En particular, en este proyecto se desarrollará el video-juego "Kingdom Rush". Este es un juego de estrategia del tipo “Defensa de Torres” (Tower Defense), en donde el jugador debe defenderse de un enemigo que intentará ingresar a nuestro terreno y destruirnos.    

En Kingdom Rush el jugador debe defender el camino de los enemigos, quienes intentarán llegar a la meta situada al final del mismo. Cada vez que un enemigo pasa la meta se descuenta una vida. Si el jugador deja pasar muchos enemigos al final del camino resultará en el fin del juego. El objetivo del jugador es, entonces, destruir a los enemigos y evitar que lleguen a este punto. Al comenzar una partida el jugador tendrá cierta cantidad de vidas que se representan en pantalla por el ícono de un corazón.    

### Torres ###

Para eliminar a los enemigos, el jugador debe ubicar a lo largo del territorio torres defensivas. Estas atalayas se podrán emplazar en ciertos lugares vacíos predeterminados llamados “Puntos Estratégicos” a lo largo del camino, los cuales varían según el nivel del juego. Las torres atacan a los enemigos en forma automática cuando estos se encuentran dentro de su alcance. Para construir una torre en un punto estratégico, se selecciona el punto y se elige el tipo de torre.    

Hay cuatro tipos de torres disponibles: Arqueros, Magos, Artillería y por último Barracas. Cada una tiene un costo de despliegue o adquisición en monedas del juego. Eliminar enemigos otorga monedas, que se acumularán, permitiendo adquirir mejoras sobre las torres (aumentando el daño que producen sobre los enemigos). Al inicio del juego, el jugador comienza con un monto fijo de monedas de regalo. La cantidad de monedas disponibles se mantiene de un nivel a otro.    

Todas las torres disparan diferentes municiones sobre los enemigos (ver Tabla a continuación) excepto la barraca que despliega soldados de infantería a su alrededor y que atacan cuerpo a cuerpo a los enemigos.

Tipo de torre | Munición
:--------------:|:--------------------------:
Arqueros      | Flecha
Magos         | Hechizo (bola de energía)
Artillería    | Bomba    

Cada tipo de torre tiene un conjunto de atributos o características, a saber: daño que provoca en los enemigos, alcance y frecuencia de disparo (en el caso de las barracas es tiempo de regeneración de los soldados –sólo cuando alguno de ellos es eliminado). Los valores de estas características pueden asignarse de forma arbitraria (utilizar [1] como referencia).    

En cualquier momento del juego, el jugador podrá mejorar cualquiera de las torres pagando el costo correspondiente, para lo cual utilizará las monedas que disponga. La mejora de cada torre asigna nuevos valores a los atributos de la misma, incluso modificando su aspecto. Por ejemplo, en la siguiente tabla se muestra la mejora que puede realizarse sobre una torre de Magos.    

  | Torre de magos |  
:--------:|:--------:|:-------:|
Nivel de la torre     | 1 | 2
Daño de municiones       | 20 | 30
Alcance | 100 | 200
Frecuencia de disparo | 3 | 2
Costo de adquisición / mejora | 100 | 160    

Cada torre podrá mejorarse hasta 3 niveles (ver [1]). Al momento de comprar una torre, si el usuario dispone de las monedas necesarias, podrá adquirir una torre de cualquiera de los niveles. Las torres adquiridas podrán ser vendidas a un 80% del costo de compra o mejora. Al seleccionar una torre emplazada, se podrá optar por mejorar o por venderla.    

### Enemigos ###

Por otra parte, existen tres tipos de enemigos: Goblins, Bandidos y Ogros, que difieren en ciertas características tales como: velocidad, energía, daño que provocan al atacar (a soldados de infantería), recompensa (que otorga en monedas) y la cantidad de vida que quitan al pasar la meta. Al igual que con las torres, los valores de estos pueden asignarse de forma arbitraria. Los enemigos aparecerán cada cierta cantidad de tiempo al principio del camino en oleadas de 3 soldados. El tiempo entre oleadas se irá decrementando en los niveles posteriores.    

Cada nivel se completa al destruir una cantidad mínima requerida de enemigos la que se irá incrementando a medida que el juego progresa.    

### Archivos de nivel ###

Los parámetros y configuración de cada nivel se leerán de archivos previamente creados que se leerán al principio de cada nivel para crear los objetos correspondientes. La configuración de cada nivel consta de:    

+ Mapa del territorio (imagen)
+ Camino (secuencia de puntos)
+ Localización de puntos estratégicos
+ Frecuencia de las oleadas de enemigos
+ Cantidad mínima de enemigos a destruir
    
### Requerimientos mínimos ###
  
El juego debe ser funcional desde el comienzo hasta el último nivel (2 como
mínimo) tal como se especificó anteriormente. Si el jugador sobrevive al último nivel, gana el juego.    

### Opcionales ###
+ Implementar efectos de sonido y música de fondo.
+ [Obligatorio para grupos de 3 integrantes] Implementar un editor de niveles utilizando una interfaz gráfica apropiada que permita asignar cada uno de los parámetros de configuración y los almacene en un archivo de nivel.

### Referencias ###
[1] [Wiki de Kingdom Rush](http://kingdomrushtd.wikia.com/wiki/)  
[2] [Video de muestra de Kingdom Rush](https://youtu.be/uxZF4s9Jook?
list=PLC8E7022B1BA805E4)  
[3] [Versión jugable de Kingdom Rush](http://armorgames.com/play/12141/kingdomrush)