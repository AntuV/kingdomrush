import java.util.Vector;
import java.awt.*;
import javax.imageio.*;
import com.entropyinteractive.*;
import java.awt.image.*;

public class PuntoEstrategico extends ObjetoGrafico {

	private Nivel nivel;
	private Boolean disponible;
	private Boolean tieneMenu;
	private Boolean sono;
	private BufferedImage animacion;
	private MenuTorre menu;
	private Boolean hayMenu;
	private Torre torre;

	public PuntoEstrategico(int x, int y, Nivel nivel){
		super("punto_estrategico.png", x, y);
		setAreaRel(+21, +5, -40,-10);
		disponible = true;
		try {
			animacion = ImageIO.read(getClass().getResource("imagenes/punto_estrategico1.png"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		tieneMenu = false;
		sono = false;
		this.nivel = nivel;
		hayMenu = false;
	}

	public Torre getTorre(){
		return torre;
	}

	public Boolean disponible(){
		return disponible;
	}

	public Boolean tieneMenu(){
		return tieneMenu;
	}

	public void dibujar(Graphics2D g, Mouse m){
		if (!hayMenu) {
			if (contiene(m)) {
				if (disponible) {
					g.drawImage(animacion, this.getX()-imagen.getWidth()/2, this.getY()-imagen.getHeight()/2, null);
				} else {
					torre.dibujar(g);
				}
			} else {
				if (disponible) {
					g.drawImage(imagen, x-imagen.getWidth()/2, y-imagen.getHeight()/2, null);
					if (sono) sono = false;
				} else {
					torre.dibujar(g);
				}
			}
		} else {
			if (tieneMenu) {
				if (disponible) {
					g.drawImage(animacion, this.getX()-imagen.getWidth()/2, this.getY()-imagen.getHeight()/2, null);
				} else {
					torre.dibujarArea(g);
					torre.dibujar(g);
				}
			} else {
				if (disponible) {
					g.drawImage(imagen, x-imagen.getWidth()/2, y-imagen.getHeight()/2, null);
				} else {
					torre.dibujar(g);
				}
			}
		}
	}

	public MenuTorre getMenu(){
		return menu;
	}

	public void actualizar(Mouse m, Boolean hayMenu){
		if (!sono && contiene(m) && !hayMenu) {
			sonido("mousehover.wav", 30);
			sono = true;
		}
		this.hayMenu = hayMenu;
	}

	public void clic(Mouse m){
		if (tieneMenu && menu.contiene(m)) {
			menu.clic(m);
		}
		if (this.contiene(m)){
			if (!tieneMenu && !hayMenu){
				sonido("mouseclic.wav", 30);
				tieneMenu = true;
				menu = new MenuTorre(this);
				nivel.setMenu(true);
			} else {
				tieneMenu = false;
				menu = null;
				nivel.setMenu(false);
			}
		} else {
			if (tieneMenu) {
				tieneMenu = false;
				menu = null;
				nivel.setMenu(false);
			}
		}
	}

	public void setTorre(int tipo) {
		int monedas = nivel.getKingdomRush().getMonedas();
		int precio = Precio.get(tipo,0);
		if (monedas >= precio) {
			switch(tipo){
				case 0:
					torre = new Arquero(this);
					break;
				case 1:
					torre = new Barracas(this);
					break;
				case 2:
					torre = new Mago(this);
					break;
				case 3:
					torre = new Mortero(this);
					break;
			}
			disponible = false;
			nivel.getKingdomRush().setMonedas(-precio);
		}
	}

	public void mejorarTorre(){
		torre.mejorar();
	}

	public void venderTorre(){
		int venta = torre.getVenta();
		if (torre instanceof Barracas) {
			((Barracas)torre).reiniciarSoldados();
		}
		disponible = true;
		torre = null;
		nivel.getKingdomRush().setMonedas((int)(venta * 0.6));
	}

	public Nivel getNivel(){
		return this.nivel;
	}
}