import java.awt.Graphics;
import java.awt.Graphics2D;
import com.entropyinteractive.Mouse;
import java.awt.image.BufferedImage;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.util.Vector;

public class Torre extends ObjetoGrafico implements IAtaque {

	protected int venta;
	protected int fuerza;
	protected int energia;
	protected int alcance;
	protected int frecuencia;
	protected int nivelTorre;
	protected PuntoEstrategico punto;
	protected Ellipse2D areaAlcance;
	protected double currTime;
    protected double creacion;
    protected BufferedImage imagenArea;

	public Torre(PuntoEstrategico p){
        super("torres/construccion.png", p.getX(), p.getY());
        punto = p;
        nivelTorre = 1;
        Musica.reproducir("construir_fx.wav");
        creacion = (double) System.nanoTime() / 1000000000.0;
	}

	public void dibujar(Graphics2D g){
		g.drawImage(imagen, this.getX()-imagen.getWidth()/2, this.getY()-imagen.getHeight()/2, null);
	}

	public int getVenta(){
		return ((int)(venta * 0.6));
	}

	public int getNivelTorre(){
		return this.nivelTorre;
	}

	public void dibujarArea(Graphics2D g){
		g.drawImage(imagenArea, (int)areaAlcance.getX(), (int)areaAlcance.getY(), null);
	}

	public void mejorar(){
	}

	public int getEnergia(){
		return energia;
	}

	public int getAlcanceX(){
		return (int)areaAlcance.getX();
	}

	public int getAlcanceY(){
		return (int)areaAlcance.getY();
	}

	public Ellipse2D getAreaAlcance(){
		return areaAlcance;
	}

	public int getAlcance(){
		return alcance;
	}

	public int getFuerza(){
		return fuerza;
	}

	public Boolean colision(Enemigo e){
		Boolean col = false;
        currTime = (double) System.nanoTime() / 1000000000.0;
        double frecDisparo = 1;
        switch(frecuencia){
        	case 1:
        		frecDisparo = 2.2;
        		break;
        	case 2:
        		frecDisparo = 1.8;
        		break;
        	case 3:
        		frecDisparo = 1.4;
        		break;
        	case 4:
        		frecDisparo = 1.2;
        		break;
        }
        if ((currTime - creacion) >= frecDisparo || this instanceof Barracas) {
	        col = (areaAlcance.intersects(e.getArea()));
	    }
	    return col;
	}

	public void atacar(Vector<Municion> municiones, Enemigo e){
		currTime = (double) System.nanoTime() / 1000000000.0;
		creacion = currTime;
		if (this instanceof Arquero) {
			Musica.reproducir("flecha_disparo_fx01.wav");
            Flecha flecha = new Flecha(x, y-imagen.getWidth()/2, e, fuerza);
            municiones.add(flecha);
        }
        if (this instanceof Mago) {
        	if (e instanceof Ogro) {
        		Rayo rayo = new Rayo(x, y-imagen.getWidth()/2, e, fuerza*2);
        		municiones.add(rayo);
        	} else {
            	Rayo rayo = new Rayo(x, y-imagen.getWidth()/2, e, fuerza);
            	municiones.add(rayo);
        	}
        }
        if (this instanceof Mortero) {
        	if (e instanceof Yeti) {
	            Bomba bomba = new Bomba(x, y-imagen.getWidth()/2, e, fuerza*2);
	            municiones.add(bomba);
        	} else {
	            Bomba bomba = new Bomba(x, y-imagen.getWidth()/2, e, fuerza);
	            municiones.add(bomba);
        	}
        }
        if (this instanceof Barracas) {
        	((Barracas)this).atacarEnemigo(e);
        }
	}

	public int getTipo(){
		int tipo = -1;
		if (this instanceof Arquero){
			tipo = 0;
		} else if (this instanceof Barracas){
			tipo = 1;
		} else if (this instanceof Mago){
			tipo = 2;
		} else if (this instanceof Mortero){
			tipo = 3;
		}
		return tipo;
	}
}