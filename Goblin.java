import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.Graphics2D;
import java.util.List;

public class Goblin extends Enemigo {

	private int numeroAnimacion = 1;

    public Goblin() {
        super("animaciones/goblins/arriba/1.png");
        energia = 40;
        recompensa =40;
        direccion = "arriba";
        penalizacion = 1;
        fuerza = 4; 
        velocidad = 2;
    }

    public void dibujar(Graphics2D g) {
        if (x == 0 && y == 0){} else {
            if (!detenido) {
            	numeroAnimacion = (numeroAnimacion >= 12) ? 1 : numeroAnimacion+1;
            	setObjeto("animaciones/goblins/" + direccion + "/" + String.valueOf(numeroAnimacion) + ".png", x, y);
            } else if (atacar) {
                numeroAnimacion = (numeroAnimacion >= 10) ? 1 : numeroAnimacion+1;
                setObjeto("animaciones/goblins/ataque/" + String.valueOf(numeroAnimacion) + ".png", x, y);
            } else {
                setObjeto("animaciones/goblins/" + direccion + "/1.png", x, y);
            }
            g.drawImage(imagen, x - imagen.getWidth()/2, y - imagen.getHeight()/2, null);
        }
    }

    public void mover(List<Point2D> puntos) {
        super.mover(puntos);
        area.setFrame(x - imagen.getWidth()/2, y - imagen.getHeight()/2, imagen.getWidth(), imagen.getHeight());
    }

}